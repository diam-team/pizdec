/*
*   PiZdEc Programming Language
*   Copyright (C) 2019 Artem Pitiurenko
*
*   License: GPLv2, see LICENSE
*/

#include <main.h>

char *pizdec_logo = "\nPPP    '  ZZZZZZ  DDDDD   EEEEE  CCCC\n"
                    "P  PP  I      ZZ  DD   D  E     C\n"
                    "PPP    I    ZZ    DD   D  EEEE  C\n"
                    "P      I  ZZ      DD   D  E     C\n"
                    "P      I  ZZZZZZ  DDDDD   EEEEE  CCCC\n\n"
                    "PiZdEc programming language, v.1.0\n"
                    "(c) DiamTeam, www: pizdec-pl.ml\n\n";
 

int main(int argc, char **argv)
{
    printf(pizdec_logo);
}
