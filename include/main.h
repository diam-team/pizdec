/*
*   PiZdEc Programming Language
*   Copyright (C) 2019 Artem Pitiurenko
*
*   License: GPLv2, see LICENSE
*/

#ifndef PiZdEc_h
#define PiZdEc_h

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv);

#endif