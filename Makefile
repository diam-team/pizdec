# F
SRCDIRS   := src
INCDIRS   := include
OBJDIR    := obj

# Tool Configuration
CC        := gcc
CFLAGS    := -c $(foreach INCDIR, $(INCDIRS), -I$(INCDIR))
LD  	  := gcc
LDFLAGS   :=


HEADERS   := $(foreach DIR, $(INCDIRS), $(wildcard $(DIR)/*.h))
SOURCES   := $(foreach DIR, $(SRCDIRS), $(wildcard $(DIR)/*.asm $(DIR)/*.c $(DIR)/*.cpp))
OBJECTS   := $(foreach OBJECT, $(patsubst %.asm, %.o, $(patsubst %.c, %.o, $(patsubst %.cpp, %.o, $(SOURCES)))), $(OBJDIR)/$(OBJECT))
TARGET    := pizdec

.PHONY : all
all : $(TARGET)
	@echo You succesfully compiled PiZdEc Programming Language!

$(TARGET) : $(OBJECTS)
	$(LD) -o $@ $+


# Generic Rules
$(OBJDIR)/%.o : %.c
	$(info ==== .c($<) -> .o($@) rule)
	$(CC) $(CFLAGS) $< -o $@
